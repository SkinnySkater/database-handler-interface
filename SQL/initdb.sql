
DROP TABLE IF EXISTS sca        CASCADE;
DROP TABLE IF EXISTS highway    CASCADE;
DROP TABLE IF EXISTS roads      CASCADE;
DROP TABLE IF EXISTS exitcity   CASCADE;
DROP TABLE IF EXISTS joincity   CASCADE;
DROP TABLE IF EXISTS registre   CASCADE;
DROP TABLE IF EXISTS closedroad CASCADE;




/*==============================================================*/
/* Table : SCA    COMPANIES                                     */
/*==============================================================*/
CREATE TABLE sca(
  code            VARCHAR(15) NOT NULL UNIQUE,
  name            VARCHAR(25) NOT NULL UNIQUE,
  turnover        INT NOT NULL,
  contract_dur    DATE NOT NULL, --DATE OF THE END OF CONTRACT

  PRIMARY KEY (code)
);


/*==============================================================*/
/* Table : HIGHWAY                                              */
/*==============================================================*/
CREATE TABLE highway(
  coda           VARCHAR(10) NOT NULL UNIQUE,
  DuKm           INT NOT NULL CHECK(DuKm > 0),
  AuKm           INT NOT NULL CHECK(AuKm > 0),
  sca_code       VARCHAR(15),

  PRIMARY KEY (coda),
  FOREIGN KEY (sca_code) REFERENCES sca(code)
);


/*==============================================================*/
/* Table : ROADS                                                */
/*==============================================================*/
CREATE TABLE roads(
  codt           VARCHAR(64) NOT NULL UNIQUE,
  coda           VARCHAR(10) NOT NULL,
  pgdukm         FLOAT NOT NULL CHECK(pgdukm > 0),
  pgaukm         FLOAT NOT NULL CHECK(pgaukm > 0),

  PRIMARY KEY (codt),
  FOREIGN KEY (coda) REFERENCES highway(coda),
  CONSTRAINT u_roads UNIQUE (codt, coda) --Make sure no duplicates roads pieces are added
);


/*==============================================================*/
/* Table : EXITCITY  -> INTERCONECTIONS                         */
/*==============================================================*/
CREATE TABLE exitcity(
  id              SERIAL,
  libelle         VARCHAR(64) NOT NULL,
  num             INT NOT NULL,
  coda            VARCHAR(10) NOT NULL,

  PRIMARY KEY(id),
  FOREIGN KEY (coda) REFERENCES highway(coda),
  CONSTRAINT e_city UNIQUE (libelle, num, coda),
  CONSTRAINT e_cit UNIQUE (num, coda)
);


/*==============================================================*/
/* Table : JOINCITY                                             */
/* Les villes Joignables par autoroutes                         */
/*==============================================================*/
CREATE TABLE joincity(
  id              SERIAL,
  zipcode         INT NOT NULL,
  name            VARCHAR(25) NOT NULL,
  coda            VARCHAR(10) NOT NULL,

  PRIMARY KEY (id),
  FOREIGN KEY (coda) REFERENCES highway(coda),
  CONSTRAINT u_jcity UNIQUE (zipcode, name, coda)
);



/*==============================================================*/
/* Table : REGISTRE                                             */
/*==============================================================*/
CREATE TABLE registre(
  num             INT NOT NULL UNIQUE,
  descrition      TEXT,

  PRIMARY KEY (num)
);


/*==============================================================*/
/* Table : CLOSEDROAD                                           */
/*==============================================================*/
CREATE TABLE closedroad(
  id             SERIAL NOT NULL UNIQUE,
  codt           VARCHAR(10) NOT NULL UNIQUE,
  b_date         DATE NOT NULL,
  e_date         DATE NOT NULL,
  descrition     TEXT,
  reg_num        INT NOT NULL, 

  PRIMARY KEY (id),
  FOREIGN KEY (reg_num) REFERENCES registre(num),
  FOREIGN KEY (codt) REFERENCES roads(codt)
);
