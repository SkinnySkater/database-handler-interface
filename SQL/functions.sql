/*==============================================================*/
/* FUNCTIONS TO UPDATE AND EDIT DATABASE FROM THE UI            */
/*==============================================================*/



/*==============================================================*/
/* FUNCTIONS TO UPDATE HIGHWAY                                  */
/*==============================================================*/

	CREATE OR REPLACE FUNCTION f_update_highway(code VARCHAR(10), dukm_ INT, aukm_ INT, sca VARCHAR(15))
RETURNS BOOLEAN AS
$$
BEGIN
  IF NOT EXISTS (SELECT coda FROM highway WHERE coda = code)  THEN
    RETURN FALSE;
  ELSE
    UPDATE highway
    SET 
    	sca_code = COALESCE(sca, sca_code),
    	DuKm   = COALESCE(dukm_, DuKm),
    	AuKm   = COALESCE(aukm_, AuKm)
    WHERE coda = code;
    RETURN TRUE;

  END IF;
  EXCEPTION WHEN OTHERS THEN
    RETURN FALSE;
END
$$ LANGUAGE plpgsql;


/*==============================================================*/
/* FUNCTIONS TO UPDATE ROADS                                    */
/*==============================================================*/
	CREATE OR REPLACE FUNCTION f_update_roads(codt_ VARCHAR(64), coda_ VARCHAR(10), pgdkm FLOAT, pgakm FLOAT)
RETURNS BOOLEAN AS
$$
BEGIN
  IF NOT EXISTS (SELECT codt FROM roads WHERE codt = codt_) THEN
    RETURN FALSE;
  ELSE
    UPDATE roads
    SET coda   = COALESCE(coda_, coda),
    	pgdukm = COALESCE(pgdkm, pgdukm),
    	pgaukm = COALESCE(pgakm, pgaukm)
    WHERE codt = codt_;
    RETURN TRUE;

  END IF;
  EXCEPTION WHEN OTHERS THEN
    RETURN FALSE;
END
$$ LANGUAGE plpgsql;


/*==============================================================*/
/* FUNCTIONS TO UPDATE INTERCONNECTIONS                         */
/*==============================================================*/
	CREATE OR REPLACE FUNCTION f_update_exitcity(name VARCHAR(64), num_ INT, coda_ VARCHAR(10))
RETURNS BOOLEAN AS
$$
BEGIN

    UPDATE exitcity
    SET num  = COALESCE(num_, num),
    	coda = COALESCE(coda_, coda)
    WHERE libelle = name;
    RETURN TRUE;

  EXCEPTION WHEN OTHERS THEN
  RAISE EXCEPTION 'ERROR: TU  ES CHIE DESSUS !!!';
    RETURN FALSE;
END
$$ LANGUAGE plpgsql;


/*==============================================================*/
/* FUNCTIONS TO UPDATE JOINABLE CITY TABLE                      */
/*==============================================================*/
	CREATE OR REPLACE FUNCTION f_update_joincity(id_ INT, zip INT, name_ VARCHAR(25), coda_ VARCHAR(10))
RETURNS BOOLEAN AS
$$
BEGIN
    UPDATE joincity
    SET name    = COALESCE(name_, name),
    	zipcode = COALESCE(zip, zipcode),
    	coda    = COALESCE(coda_, coda)
    WHERE id = id_;
    RETURN TRUE;

  EXCEPTION WHEN OTHERS THEN
    RETURN FALSE;
END
$$ LANGUAGE plpgsql;





/*==============================================================*/
/* FUNCTIONS TO GET INFO FROM A GIVEN CITY                      */
/*==============================================================*/
	CREATE OR REPLACE FUNCTION f_get_info_city(name_ VARCHAR(25))
RETURNS TABLE(coda VARCHAR(10), libelle VARCHAR(64)) AS
$$
BEGIN
  RETURN QUERY SELECT joincity.coda AS coda, exitcity.libelle AS libelle FROM joincity
  			   INNER JOIN highway  ON highway.coda = joincity.coda
  			   INNER JOIN exitcity ON exitcity.coda = highway.coda
  			   WHERE joincity.name = name_;
  
END
$$ LANGUAGE plpgsql;





/*==============================================================*/
/* FUNCTIONS TO GET DIFF IN MONTH OF 2 DATES                    */
/*==============================================================*/
CREATE OR REPLACE FUNCTION f_my_date_diff(_start_id DATE, _end_id DATE)
  RETURNS VARCHAR(25) AS
$$
BEGIN

	RETURN CONCAT((DATE_PART('year', _end_id) - DATE_PART('year', _start_id)) * 12 +
              (DATE_PART('month', _end_id) - DATE_PART('month', _start_id)), ' Mois');

END
$$ LANGUAGE plpgsql;



/*==============================================================*/
/* FUNCTIONS TO GET INFO FROM A GIVEN CITY                      */
/*==============================================================*/
	CREATE OR REPLACE FUNCTION f_get_sca_roads_info(sca_n VARCHAR(15))
RETURNS TABLE(coda VARCHAR(10), remaining_time VARCHAR(25), earn FLOAT) AS
$$
DECLARE
cdate DATE;
BEGIN
cdate = (SELECT current_date);
  RETURN QUERY SELECT roads.codt
  					, f_my_date_diff(cdate, sca.contract_dur)
  					, 100 * (roads.pgaukm + roads.pgdukm) AS earn
  			   FROM roads
  			   INNER JOIN highway  ON highway.coda = roads.coda
  			   INNER JOIN sca 	   ON sca.code = highway.sca_code
  			   WHERE sca.code = sca_n;
END
$$ LANGUAGE plpgsql;



/*==============================================================*/
/* FUNCTIONS TO GET INFO FROM A GIVEN CITY                      */
/*==============================================================*/
	CREATE OR REPLACE FUNCTION f_find_path(city1 VARCHAR(25), city2 VARCHAR(25))
RETURNS TABLE(codt VARCHAR(64)) AS
$$
BEGIN
  RETURN QUERY (SELECT roads.codt FROM roads
  			   INNER JOIN highway  ON highway.coda = roads.coda
  			   INNER JOIN joincity ON joincity.coda = highway.coda
  			   INNER JOIN exitcity ON exitcity.coda = highway.coda
  			   WHERE exitcity.libelle = city1 OR exitcity.libelle = city2
  			   EXCEPT
  			   SELECT closedroad.codt FROM closedroad);
  
END
$$ LANGUAGE plpgsql;