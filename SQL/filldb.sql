--(Re)Create the tables
\i initdb.sql
\i functions.sql
--ADD PERMANENTLY FRENCH DATESTYLE TO THE OPEN SESSION
SET datestyle TO "SQL, DMY";
--set all right to me ; ]
CREATE USER oscar WITH PASSWORD '123';
ALTER ROLE oscar SUPERUSER ;
CREATE USER admin WITH PASSWORD 'carotte';
--sca
INSERT INTO sca VALUES
('EL-ED5-TY', 'EL Cartel', 2500450, '02-12-2018'),
('RWAY-78-D4', 'ROADWAY', 7599000, '15-07-2017'),
('XPR-12', 'GLOBAL XPRS', 42600000, '22-11-2027'),
('FRR-1548', 'France Route', 599004, '04-01-2019');


--HIGHWAY	
INSERT INTO highway VALUES
('A1', 155, 265,'XPR-12'),
('A5', 15, 25, 'XPR-12'),
('A32', 5, 17, 'XPR-12'),
('A18', 12, 13, 'XPR-12'),
('A2', 205, 743, 'FRR-1548'),
('A7', 157, 677, 'FRR-1548'),
('A12', 107, 672, 'FRR-1548'),
('A3', 12, 67, 'FRR-1548'),
('A55', 157, 785, 'EL-ED5-TY'),
('A15', 147, 465, 'EL-ED5-TY'),
('A19', 41, 46, 'EL-ED5-TY'),
('A6', 11, 167, 'RWAY-78-D4');


--ROADS
INSERT INTO roads VALUES
('A1-3-6D', 'A1', 50, 14.3),
('A5-1-4D', 'A5', 42.4, 18.4),
('A32-3-7D', 'A32', 87.002, 198.8),
('A18-8-01D', 'A18', 5, 89.0),
('A1-01-B6D', 'A1', 19.7, 155),
('A55-4-1D', 'A55', 5.5, 6.8),
('A55-B4-B2D', 'A55', 65.4, 52.4),
('A55B-2B4-2B1D', 'A55', 44.45, 55),
('A12-21-87D', 'A12', 57, 58.25),
('A7-2-9D', 'A7', 78.7, 12.54),
('BA7-B2-B9D', 'A7', 1.254, 1.347),
('BA19-78-7A', 'A19', 12.54, 13.47),
('BA6-B2-B9D', 'A6', 4.5, 6.78),
('AA15-ZZ2-77W', 'A15', 445, 778),
('AA2T-LD5-7PO', 'A2', 8.96, 9.96),
('AA3T-FEF5-5I', 'A3', 1, 4.62),
('AA3T-F5E-22A', 'A3', 5.68, 7.41);


--EXIT CITY
INSERT INTO exitcity VALUES
(DEFAULT, 'Amiens Sud', 1, 'A1'),
(DEFAULT, 'Nice Cenre', 2, 'A1'),
(DEFAULT, 'Paris Sud Ouest', 3, 'A3'),
(DEFAULT, 'Le Havre', 33, 'A3'),
(DEFAULT, 'Paris Sud Est', 4, 'A5'),
(DEFAULT, 'Orléan', 44, 'A5'),
(DEFAULT, 'Pays de la Loire', 5, 'A2'),
(DEFAULT, 'Saran', 55, 'A2'),
(DEFAULT, 'Vichy Nord', 47, 'A7'),
(DEFAULT, 'Bordeau', 69, 'A6'),
(DEFAULT, 'Marseille', 696, 'A6'),
(DEFAULT, 'Bordeau Est', 57, 'A7'),
(DEFAULT, 'Rennes', 49, 'A55'),
(DEFAULT, 'Pau', 492, 'A55'),
(DEFAULT, 'Brest', 9, 'A32'),
(DEFAULT, 'Monpellier', 99, 'A32'),
(DEFAULT, 'Tours', 132, 'A19'),
(DEFAULT, 'Dijon', 232, 'A19'),
(DEFAULT, 'Reims', 432, 'A19'),
(DEFAULT, 'Lyon', 98, 'A18'),
(DEFAULT, 'Macon', 108, 'A18'),
(DEFAULT, 'Auxerre', 208, 'A18'),
(DEFAULT, 'Rouen', 308, 'A18'),
(DEFAULT, 'Metz', 63, 'A15'),
(DEFAULT, 'Lille', 636, 'A15');



--joincity
INSERT INTO joincity VALUES
(DEFAULT, 45002, 'Besancon', 'A1'),
(DEFAULT, 65320, 'Belfort', 'A1'),
(DEFAULT, 12065, 'Nancy', 'A1'),
(DEFAULT, 75000, 'Paris', 'A3'),
(DEFAULT, 75000, 'Paris', 'A5'),
(DEFAULT, 86500, 'Nice', 'A1'),
(DEFAULT, 42358, 'Le Havre', 'A3'),
(DEFAULT, 13057, 'Marseille', 'A6'),
(DEFAULT, 24057, 'Bordeau', 'A6'),
(DEFAULT, 00458, 'Avallon', 'A6'),
(DEFAULT, 65894, 'Pays de la Loire', 'A2'),
(DEFAULT, 32657, 'Saran', 'A2'),
(DEFAULT, 52580, 'Arras', 'A2'),
(DEFAULT, 22354, 'Berck', 'A32'),
(DEFAULT, 50089, 'Brest', 'A32'),
(DEFAULT, 10005, 'Monpellier', 'A32'),
(DEFAULT, 80304, 'Vichy', 'A7'),
(DEFAULT, 00087, 'Bordeau', 'A7'),
(DEFAULT, 20047, 'Evreux', 'A7'),
(DEFAULT, 96001, 'Pau', 'A55'),
(DEFAULT, 76021, 'Rennes', 'A55'),
(DEFAULT, 62000, 'Calais', 'A55');


--registre
INSERT INTO registre VALUES
(258, 'Aie Aie Aie! C est la cata, ya des nids de poules partout, c est pas possible ça !'),
(12, 'Déterioration des signalitiques sur une trentaine de kilomètre');

--closedroad
INSERT INTO closedroad VALUES
(DEFAULT, 'A1-3-6D', '24-03-2017', '02-12-2017', '30ene de kilomètre impraticable va falloir bosser si on veut cadeau de papa Noel!',258);