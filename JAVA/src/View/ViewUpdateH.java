package View;

import Controller.DialogUpHController;
import Model.Model;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Julien Lugard on 4/28/17.
 */
public class ViewUpdateH extends JDialog
{
    public ViewUpdateH(JFrame parent, String title)
    {
        super(parent, title, true);
        if (parent != null)
        {
            Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
            setLocation((screenSize.width - getWidth() - parent.getX()) / 2,
                    (screenSize.height - getHeight() - parent.getY()) / 2);
        }

        setPreferredSize(new Dimension(350, 250));

        final ImageIcon backgroundImage =  new ImageIcon("src/View/splash/images/splash2.jpg");
        JPanel p = new JPanel(new BorderLayout()) {
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                g.drawImage(backgroundImage.getImage(), 0, 0, getWidth(), getHeight(), this);
            }
        };

        //Create component here
        JLabel primarykey = new JLabel("Select Primary Key:");
        primarykey.setForeground(Color.WHITE);
        JLabel dukm = new JLabel("Enter the Dukm row(INT):");
        dukm.setForeground(Color.WHITE);
        JLabel aukm = new JLabel("Enter the Aukm row(INT):");
        aukm.setForeground(Color.WHITE);
        JLabel sca = new JLabel("Enter the sca_code row(STRING):");
        sca.setForeground(Color.WHITE);

        JComboBox pkBoxH = new JComboBox();
        Model.populatePkBox(pkBoxH, Model.table.H);

        JTextField txtFDK = new  JTextField();
        JTextField txtFAK = new  JTextField();
        JTextField txtSCA = new  JTextField();

        JButton update = new JButton("Update");

        //Create dialogs controller
        DialogUpHController controller = new DialogUpHController(pkBoxH, txtFDK, txtFAK, txtSCA, update, this);
        update.addActionListener(controller);


        JPanel buttonPane = new JPanel();
        buttonPane.add(update);
        p.setLayout(new GridLayout(2, 3, 5, 5));
        p.add(primarykey);
        p.add(pkBoxH);
        p.add(dukm);
        p.add(txtFDK);
        p.add(aukm);
        p.add(txtFAK);
        p.add(sca);
        p.add(txtSCA);
        p.setLayout(new GridLayout(0, 1, 0, 0));
        setContentPane(p);
        add(update, BorderLayout.SOUTH);

        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        pack();
        setVisible(true);
        setResizable(false);
    }
}
