package View;

import Controller.MainWindowController;
import Controller.ToolBarController;
import Controller.ToolBarController2;
import Model.Model;
import MyUtil.Constant;
import MyUtil.MyTextArea;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

/**
 * Created by Julien Lugard on 4/27/17.
 */
public class View extends JFrame
{
    //ATTRIBUTES
    private static int width  = 1200;
    private static int height =  800;
    private JDialog conectD;
    private JDialog upH;
    private static MainWindowController controller;
    private static ToolBarController2 tbc;
    private JTextArea displayArea = new MyTextArea(width, height);
    private JLabel statusBar;

    //CONSTRUCTOR
    public View()
    {
        super("ROUTES DATABASE");

        statusBar = new JLabel(Constant.disconnected);
        statusBar.setPreferredSize(new Dimension(width, 30));

        displayArea.setForeground(Color.WHITE);
        displayArea.setBackground(new Color(1,1,1, (float) 0.01));

        JScrollPane scroll = new JScrollPane (displayArea, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED
                , JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scroll.setPreferredSize(new Dimension(width, height));
        scroll.getViewport().setScrollMode(JViewport.SIMPLE_SCROLL_MODE);
        scroll.setBackground(Color.BLACK);

        JPanel toolP = new JPanel();
        toolP.setBackground(new Color(64, 64, 64));
        toolP.setLayout(new BoxLayout(toolP, BoxLayout.Y_AXIS));
        JToolBar bar1 = getToolBar();
        JToolBar bar2 = getToolBar2();
        bar2.setAlignmentX(0);
        bar1.setAlignmentX(0);
        toolP.add(bar1);
        toolP.add(bar2);
        add(toolP, BorderLayout.NORTH);
        add(scroll, BorderLayout.CENTER);
        add(statusBar, BorderLayout.SOUTH);


        pack();
        //set options
        setResizable(true);
        setPreferredSize(new Dimension(width, height));
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation((screenSize.width - getWidth()) / 2,
                (screenSize.height - getHeight()) / 2);
        Image im = Toolkit.getDefaultToolkit().getImage("src/View/splash/images/oscarotte.gif");
        setIconImage(im);
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        setVisible(true);
        setFocusable(false);


        //Display the connection dialog right after
        controller = new MainWindowController();
        controller.setStatusBar(statusBar);
        controller.setMainFraame(this);



        this.addWindowListener(controller);
        conectD = new ViewConnectDialog(this, "Database Connection"
                , "Please Enter the credentials", controller);

        //Set the model properties
        Model.dispA = displayArea;
    }

    public static JToolBar getToolBar()
    {
        JToolBar toolbar = new JToolBar();
        ActionListener listener = new ToolBarController();
        toolbar.setRollover(false);
        toolbar.setVisible(true);
        toolbar.setFloatable(false);
        toolbar.setBackground(new Color(64, 64, 64));

        toolbar.addSeparator();
        JLabel label = new JLabel("Table content :");
        label.setForeground(Color.WHITE);
        toolbar.add(label);
        toolbar.addSeparator();
        JButton button = new JButton("HIGHWAYS");
        button.addActionListener(listener);
        toolbar.add(button);
        toolbar.addSeparator();
        button = new JButton("ROADWAYS");
        button.addActionListener(listener);
        toolbar.add(button);

        toolbar.addSeparator();
        button = new JButton("INTERCONNECTIONS");
        button.addActionListener(listener);
        toolbar.add(button);

        toolbar.addSeparator();
        button = new JButton("SERVICED CITIES");
        button.addActionListener(listener);
        toolbar.add(button);

        toolbar.add(Box.createHorizontalGlue());

        button = new JButton("CLEAR");
        button.addActionListener(listener);
        toolbar.add(button);

        toolbar.addSeparator();

        return toolbar;
    }

    public JToolBar getToolBar2()
    {
        JToolBar toolbar = new JToolBar();
        ToolBarController2 listener = new ToolBarController2();
        listener.setParent(this);

        toolbar.setRollover(false);
        toolbar.setVisible(true);
        toolbar.setFloatable(false);
        toolbar.setBackground(new Color(64, 64, 64));

        toolbar.addSeparator();
        JLabel label = new JLabel("Update Table :");
        label.setForeground(Color.WHITE);
        toolbar.add(label);
        toolbar.addSeparator();

        JButton button = new JButton("HIGHWAYS");
        button.addActionListener(listener);
        toolbar.add(button);
        toolbar.addSeparator();

        button = new JButton("ROADWAYS");
        button.addActionListener(listener);
        toolbar.add(button);

        toolbar.addSeparator();
        button = new JButton("INTERCONNECTIONS");
        button.addActionListener(listener);
        toolbar.add(button);

        toolbar.addSeparator();
        button = new JButton("SERVICED CITIES");
        button.addActionListener(listener);
        toolbar.add(button);

        toolbar.add(Box.createHorizontalGlue());

        button = new JButton("GET INFO CITY");
        button.addActionListener(listener);
        toolbar.add(button);

        toolbar.addSeparator();

        button = new JButton("SCA ROADS");
        button.addActionListener(listener);
        toolbar.add(button);

        toolbar.addSeparator();
        button = new JButton("SEARCH");
        button.addActionListener(listener);
        toolbar.add(button);

        toolbar.addSeparator();

        return toolbar;
    }
}
