package View;

import Controller.DialogUpRController;
import Model.Model;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Julien Lugard on 4/29/17.
 */
public class ViewUpdateR extends JDialog
{
    public ViewUpdateR(JFrame parent, String title)
    {
        super(parent, title, true);
        if (parent != null)
        {
            Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
            setLocation((screenSize.width - getWidth() - parent.getX()) / 2,
                    (screenSize.height - getHeight() - parent.getY()) / 2);
        }

        setPreferredSize(new Dimension(350, 250));

        final ImageIcon backgroundImage =  new ImageIcon("src/View/splash/images/splash2.jpg");
        JPanel p = new JPanel(new BorderLayout()) {
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                g.drawImage(backgroundImage.getImage(), 0, 0, getWidth(), getHeight(), this);
            }
        };

        //Create component here
        JLabel primarykey = new JLabel("Select Primary Key:");
        primarykey.setForeground(Color.WHITE);
        JLabel coda = new JLabel("Enter the coda row(STRING):");
        coda.setForeground(Color.WHITE);
        JLabel pgdukm = new JLabel("Enter the PgDuKm row(FLOAT):");
        pgdukm.setForeground(Color.WHITE);
        JLabel pgaukm = new JLabel("Enter the PgAuKm row(FLOAT):");
        pgaukm.setForeground(Color.WHITE);

        JComboBox pkBoxH = new JComboBox();
        Model.populatePkBox(pkBoxH, Model.table.R);

        JTextField txt1 = new  JTextField();
        JTextField txt2 = new  JTextField();
        JTextField txt3 = new  JTextField();

        JButton update = new JButton("Update");

        //Create dialogs controller
        DialogUpRController controller = new DialogUpRController(pkBoxH, txt1, txt2, txt3, update, this);
        update.addActionListener(controller);


        JPanel buttonPane = new JPanel();
        buttonPane.add(update);
        p.setLayout(new GridLayout(2, 3, 5, 5));
        p.add(primarykey);
        p.add(pkBoxH);
        p.add(coda);
        p.add(txt1);
        p.add(pgdukm);
        p.add(txt2);
        p.add(pgaukm);
        p.add(txt3);
        p.setLayout(new GridLayout(0, 1, 0, 0));
        setContentPane(p);
        add(update, BorderLayout.SOUTH);

        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        pack();
        setVisible(true);
        setResizable(false);
    }
}
