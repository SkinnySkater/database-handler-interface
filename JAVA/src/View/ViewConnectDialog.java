package View;

import Controller.MainWindowController;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Julien Lugard on 4/27/17.
 */
public class ViewConnectDialog extends JDialog
{
    public ViewConnectDialog(JFrame parent, String title, String message, MainWindowController controller)
    {
        super(parent, title, true);
        if (parent != null)
        {
            Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
            setLocation((screenSize.width - getWidth() - parent.getX()) / 2,
                    (screenSize.height - getHeight() - parent.getY()) / 2);
        }

        setPreferredSize(new Dimension(250, 150));

        final ImageIcon backgroundImage =  new ImageIcon("src/View/splash/images/splash4.jpg");
        JPanel p = new JPanel(new BorderLayout()) {
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                g.drawImage(backgroundImage.getImage(), 0, 0, getWidth(), getHeight(), this);
            }
        };

        JTextField user = new JTextField("user");
        JPasswordField pwd = new JPasswordField("password");
        JButton button = new JButton("Log in");
        JLabel userl = new JLabel("Username:");
        userl.setForeground(Color.WHITE);
        JLabel pwdl  = new JLabel("Password:");
        pwdl.setForeground(Color.WHITE);

        //Create dialogs controller
        controller.setConnectionComponent(this, user, pwd, button);
        button.addActionListener(controller);


        JPanel messagePane = new JPanel();
        messagePane.add(new JLabel(message));
        getContentPane().add(messagePane);

        p.setLayout(new GridLayout(3, 2, 5, 5));
        p.add(userl);
        p.add(user);
        p.add(pwdl);
        p.add(pwd);
        p.setLayout(new GridLayout(0, 1, 0, 0));
        p.add(button);


        setContentPane(p);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        pack();
        setVisible(true);
        setResizable(false);
    }

}
