package View;

import Controller.DialogUpIController;
import Model.Model;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Julien Lugard on 4/29/17.
 */
public class ViewUpdateI extends JDialog
{
    public ViewUpdateI(JFrame parent, String title)
    {
        super(parent, title, true);
        if (parent != null)
        {
            Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
            setLocation((screenSize.width - getWidth() - parent.getX()) / 2,
                    (screenSize.height - getHeight() - parent.getY()) / 2);
        }

        setPreferredSize(new Dimension(350, 250));

        final ImageIcon backgroundImage =  new ImageIcon("src/View/splash/images/splash2.jpg");
        JPanel p = new JPanel(new BorderLayout()) {
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                g.drawImage(backgroundImage.getImage(), 0, 0, getWidth(), getHeight(), this);
            }
        };

        //Create component here
        JLabel primarykey = new JLabel("Select City Key:");
        primarykey.setForeground(Color.WHITE);
        JLabel num = new JLabel("Enter the num row(INT):");
        num.setForeground(Color.WHITE);
        JLabel coda = new JLabel("Enter the coda row(STRING):");
        coda.setForeground(Color.WHITE);

        JComboBox pkBoxH = new JComboBox();
        Model.populatePkBox(pkBoxH, Model.table.I);

        JTextField txt1 = new  JTextField();
        JTextField txt2 = new  JTextField();

        JButton update = new JButton("Update");

        //Create dialogs controller
        DialogUpIController controller = new DialogUpIController(pkBoxH, txt1, txt2, update, this);
        update.addActionListener(controller);


        JPanel buttonPane = new JPanel();
        buttonPane.add(update);
        p.setLayout(new GridLayout(2, 2, 5, 5));
        p.add(primarykey);
        p.add(pkBoxH);
        p.add(num);
        p.add(txt1);
        p.add(coda);
        p.add(txt2);
        p.setLayout(new GridLayout(0, 1, 0, 0));
        setContentPane(p);
        add(update, BorderLayout.SOUTH);

        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        pack();
        setVisible(true);
        setResizable(false);
    }
}
