package View;

/**
 * Created by Julien Lugard on 4/27/17.
 */


import javax.swing.*;
import java.awt.*;

public class SplashScreen extends JFrame
{
    //ATRIBUTE
    private JLabel imglabel;
    private ImageIcon img;
    private static JProgressBar loadbar;

    public SplashScreen()
    {
        super("Splash");
        int w, h, thick;
        thick = 25;
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        img = new ImageIcon("src/View/splash/images/splash4.jpg");
        imglabel = new JLabel(img);
        w = (int) screenSize.getWidth() / 3;
        h = (int) screenSize.getHeight() / 3;
        this.setSize(w, h);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setUndecorated(true);
        add(imglabel);
        setLayout(null);
        loadbar = new JProgressBar();
        loadbar.setMinimum(0);
        loadbar.setMaximum(100);
        loadbar.setStringPainted(true);
        loadbar.setForeground(Color.BLACK);
        imglabel.setBounds(0, 0, w, h);
        this.add(loadbar);
        loadbar.setPreferredSize(new Dimension(h, 30));
        loadbar.setBounds(0, h - thick, w, thick);
        runThread();

    }

    private void runThread()
    {
        Thread t = new Thread()
        {
            public void run()
            {
                int i = 0;
                while (i <= 100)
                {
                    loadbar.setValue(i);
                    Bullshit(i);
                    try
                    {
                        sleep(90);
                    }
                    catch (InterruptedException ex)
                    {
                        ex.printStackTrace();
                    }
                    i++;
                }
            }
        };
        t.start();
    }
    private void Bullshit(int i)
    {
        if (i < 10)
            loadbar.setString("Initializing main component" + " " + i + "%");
        else if (i < 15)
            loadbar.setString("Import Swing component" + " " + i + "%");
        else if (i < 20)
            loadbar.setString("Building software Interface" + " " + i + "%");
        else if (i < 22)
            loadbar.setString("Set up Action dependencies " + i + "%");
        else if (i < 28)
            loadbar.setString("JDBC Detection " + i + "%");
        else if (i < 37)
            loadbar.setString("Loading projects " + i + "%");
        else if (i < 56)
            loadbar.setString("Install Network dependencies " + i + "%");
        else if (i < 66)
            loadbar.setString("Install Graphical dependencies " + i + "%");
        else if (i < 76)
            loadbar.setString("Load JDBC dependencies " + i + "%");
        else if (i < 95)
            loadbar.setString("Fire up... " + i + "%");
    }
}