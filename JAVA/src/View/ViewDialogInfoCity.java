package View;

import Controller.DialogInfoCity;
import Model.Model;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Julien Lugard on 4/29/17.
 */
public class ViewDialogInfoCity extends JDialog
{
    public ViewDialogInfoCity(JFrame parent, String title)
    {
        super(parent, title, true);
        if (parent != null)
        {
            Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
            setLocation((screenSize.width - getWidth() - parent.getX()) / 2,
                    (screenSize.height - getHeight() - parent.getY()) / 2);
        }

        setPreferredSize(new Dimension(250, 150));

        final ImageIcon backgroundImage =  new ImageIcon("src/View/splash/images/splash2.jpg");
        JPanel p = new JPanel(new BorderLayout()) {
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                g.drawImage(backgroundImage.getImage(), 0, 0, getWidth(), getHeight(), this);
            }
        };

        //Create component here
        JLabel primarykey = new JLabel("Select The city :");
        primarykey.setForeground(Color.WHITE);


        JComboBox pkBoxH = new JComboBox();
        Model.populatePkBox(pkBoxH, Model.table.I);

        JTextField txt1 = new  JTextField();
        JTextField txt2 = new  JTextField();
        JTextField txt3 = new  JTextField();

        JButton update = new JButton("Update");

        //Create dialogs controller
        DialogInfoCity controller = new DialogInfoCity(pkBoxH, update, this);
        update.addActionListener(controller);


        JPanel buttonPane = new JPanel();
        buttonPane.add(update);
        p.setLayout(new GridLayout(0,1));
        p.add(primarykey);
        p.add(pkBoxH);
        setContentPane(p);
        add(update, BorderLayout.SOUTH);

        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        pack();
        setVisible(true);
        setResizable(false);
    }
}
