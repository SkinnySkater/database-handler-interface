package Model;


import MyUtil.Constant;

import javax.swing.*;
import java.sql.*;

/**
 * Created by Julien Lugard on 4/27/17.
 */
public class Model
{
    //ATTRIBUTES
    public static enum  table {H, R, I, J, S}
    private String status;
    public static boolean isConnected = false;
    public static Connection c = null;
    public static String display = "";

    public static JTextArea dispA;

    public Model(){}

    public void setTextArea(JTextArea display)
    {
        this.dispA = display;
    }

    /**
     * Try to connect to the database
     * @param user
     * @param pwd
     * @return Boolean
     * @throws Exception
     */
    public static boolean isIsConnected(String user, String pwd) throws Exception {
        String s = null;
        try {
            c = DriverManager.getConnection(Constant.DB_URL, user, pwd);
            System.out.println(c.getAutoCommit());
            s = c.getCatalog();
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println(e.getClass().getName()+": "+e.getMessage());
            return false;
        }
        System.out.println("Opened database successfully, with version : " + s);
        Constant.connected += s;
        isConnected = true;
        return true;
    }

    public static void Clear()
    {
        display = "";
        dispA.setText(display);
    }

    public static void displayHighways()
    {
        Statement stmt = null;
        //Just in case reinitialize the string to display
        display += "\n\n\n------------------------------------------------------------------\n";
        try{
            System.out.println("Creating statement...");
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery(Constant.displayH);
            while(rs.next()){
                //Retrieve by column name
                String coda  = rs.getString("coda");
                int dukm = rs.getInt("DuKm");
                int aukm = rs.getInt("AuKm");
                String sca = rs.getString("sca_code");

                //Display values
                display += "codeA: " + coda;
                display += ", DuKm: " + dukm;
                display += ", AuKm: " + aukm;
                display += ", sca_code: " + sca;
                display += "\n";
            }
            dispA.setText(display);
            rs.close();
        }
        catch (SQLException s)
        {
            s.printStackTrace();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

     public static void displayRoads()
    {
        Statement stmt = null;
        //Just in case reinitialize the string to display
        display += "\n\n\n------------------------------------------------------------------\n";
        try{
            System.out.println("Creating statement...");
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery(Constant.displayR);
            while(rs.next()){
                //Retrieve by column name
                String codt  = rs.getString("codt");
                String coda  = rs.getString("coda");
                float pgdukm = rs.getFloat("pgdukm");
                float pgaukm = rs.getFloat("pgaukm");

                //Display values
                display += "codeT: " + codt;
                display += ", codeA: " + coda;
                display += ", pgDukm: " + pgdukm;
                display += ", pgAuKm: " + pgaukm;
                display += "\n";
            }
            dispA.setText(display);
            rs.close();
        }
        catch (SQLException s)
        {
            s.printStackTrace();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

     public static void displayInter()
    {
        Statement stmt = null;
        //Just in case reinitialize the string to display
        display += "\n\n\n------------------------------------------------------------------\n";
        try{
            System.out.println("Creating statement...");
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery(Constant.displayI);
            while(rs.next()){
                //Retrieve by column name
                int id = rs.getInt("id");
                String libel  = rs.getString("libelle");
                int num = rs.getInt("num");
                String coda  = rs.getString("coda");

                //Display values
                display += "id: " + id;
                display += ", libelle: " + libel;
                display += ", num: " + num;
                display += ", coda: " + coda;
                display += "\n";
            }
            dispA.setText(display);
            rs.close();
        }
        catch (SQLException s)
        {
            s.printStackTrace();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }



    public static void getInfoFromCity(String city)
    {
        Statement stmt = null;
        //Just in case reinitialize the string to display
        display += "\n\n\n------------------------------------------------------------------\n";
        try{
            System.out.println("Creating statement...");
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery(Constant.getInfoCity(city));
            while(rs.next()){
                //Retrieve by column name
                String coda  = rs.getString("coda");
                String libel  = rs.getString("libelle");

                //Display values
                display += "libelle: " + libel;
                display += ", coda: " + coda;
                display += "\n";
            }
            dispA.setText(display);
            rs.close();
        }
        catch (SQLException s)
        {
            s.printStackTrace();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }


    public static void getInfoSca(String name)
    {
        Statement stmt = null;
        //Just in case reinitialize the string to display
        display += "\n\n\n------------------------------------------------------------------\n";
        try{
            System.out.println("Creating statement...");
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery(Constant.getScaInfo(name));
            while(rs.next()){
                //Retrieve by column name
                String coda  = rs.getString("codt");
                String duration  = rs.getString("duration");
                Float earn = rs.getFloat("earn");

                //Display values
                display += "codT: " + coda;
                display += ", duration: " + duration;
                display += ", earn: " + earn;
                display += "\n";
            }
            dispA.setText(display);
            rs.close();
        }
        catch (SQLException s)
        {
            s.printStackTrace();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }


    public static void getInfoSearch(String city1, String city2)
    {
        Statement stmt = null;
        //Just in case reinitialize the string to display
        display += "\n\n\n------------------------------------------------------------------\n";
        try{
            System.out.println("Creating statement...");
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery(Constant.getSearchInfo(city1, city2));
            int i = 1;
            while(rs.next()){
                //Retrieve by column name
                String codt  = rs.getString("codt");

                //Display values
                display += i +")codT : " + codt;
                display += "\n";
                i++;
            }
            dispA.setText(display);
            rs.close();
        }
        catch (SQLException s)
        {
            s.printStackTrace();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }


    public static void displaySc()
    {
        Statement stmt = null;
        //Just in case reinitialize the string to display
        display += "\n\n\n------------------------------------------------------------------\n";
        try{
            System.out.println("Creating statement...");
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery(Constant.displayJ);
            while(rs.next()){
                //Retrieve by column name
                int id = rs.getInt("id");
                int zip = rs.getInt("zipcode");
                String name  = rs.getString("name");
                String coda  = rs.getString("coda");

                //Display values
                display += "id: " + id;
                display += ", zipcode: " + zip;
                display += ", name: " + name;
                display += ", coda: " + coda;
                display += "\n";
            }
            dispA.setText(display);
            rs.close();
        }
        catch (SQLException s)
        {
            s.printStackTrace();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }


    /**
     * Fill th echoosen combobox for the update dialog windows
     * @param box
     * @param table
     */
    public static void populatePkBox(JComboBox box, table table)
    {
        Statement stmt = null;
        //Just in case reinitialize the string to display
        try{
            System.out.println("Creating statement...");
            stmt = c.createStatement();
            ResultSet rs;
            //Choose the right query
            switch (table)
            {
                case H:
                    rs = stmt.executeQuery(Constant.getPkH);
                    while (rs.next())
                        box.addItem(rs.getString("coda"));
                    break;
                case R:
                    rs = stmt.executeQuery(Constant.getPkR);
                    while (rs.next())
                        box.addItem(rs.getString("codt"));
                    break;
                case I:
                    rs = stmt.executeQuery(Constant.getPkI);
                    while (rs.next())
                        box.addItem(rs.getString("libelle"));
                    break;
                case J:
                    rs = stmt.executeQuery(Constant.getPkJ);
                    while (rs.next())
                        box.addItem(rs.getInt("id"));
                    break;
                case S:
                    rs = stmt.executeQuery(Constant.getPkS);
                    while (rs.next())
                        box.addItem(rs.getString("name"));
                    break;
                default://just to avoid uninitializ error but it will never go here
                    rs = stmt.executeQuery(Constant.getPkH);
            }
            rs.close();
        }
        catch (SQLException s)
        {
            s.printStackTrace();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }


    /**
     * UPDATE TABLES METHODS
     */
    public static boolean UpdateH(String code, int dkm, int akm, String sca)
    {
        boolean result = false;
        try{
            System.out.println("Creating Callable statement...");

            c.setAutoCommit(false);
            CallableStatement proc = c.prepareCall(Constant.getCallH);
            proc.registerOutParameter(1, Types.BOOLEAN);
            proc.setString(2, code);
            proc.setInt(3, dkm);
            proc.setInt(4, akm);
            proc.setString(5, sca);
            proc.execute();

            result = proc.getBoolean(1);
            proc.close();
            //put it back to true
            c.setAutoCommit(true);
        }
        catch (SQLException s) {
            s.printStackTrace();
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return result;
    }

      /**
     * UPDATE TABLES ROADS METHODS
     */
    public static boolean UpdateR(String codt, String coda, float pgdkm, float pgakm)
    {
        boolean result = false;
        try{
            System.out.println("Creating Callable statement...");

            c.setAutoCommit(false);
            CallableStatement proc = c.prepareCall(Constant.getCallR);
            proc.registerOutParameter(1, Types.BOOLEAN);
            proc.setString(2, codt);
            proc.setString(3, coda);
            proc.setFloat(4, pgdkm);
            proc.setFloat(5, pgakm);
            proc.execute();

            result = proc.getBoolean(1);
            proc.close();
            //put it back to true
            c.setAutoCommit(true);
        }
        catch (SQLException s) {
            s.printStackTrace();
            return false;
        }
        catch (Exception e){
            e.printStackTrace();
            return false;
        }
        return result;
    }

     /**
     * UPDATE TABLES INTERCONNECTIONS METHODS
     */
    public static boolean UpdateI(String name, int num, String coda)
    {
        boolean result = false;
        try{
            System.out.println("Creating Callable statement...");

            c.setAutoCommit(true);
            CallableStatement proc = c.prepareCall(Constant.getCallI);
            proc.registerOutParameter(1, Types.BOOLEAN);
            proc.setString(2, name);
            proc.setInt(3, num);
            proc.setString(4, coda);
            proc.execute();
            result = proc.getBoolean(1);
            proc.close();
            //put it back to true
            c.setAutoCommit(true);
        }
        catch (SQLException s) {
            s.printStackTrace();
            return false;
        }
        catch (Exception e){
            e.printStackTrace();
            return false;
        }
        return result;
    }

    /**
     * UPDATE TABLES JOINED CITIES METHODS
     */
    public static boolean UpdateJ(int id, int zip, String name, String coda)
    {
        boolean result = false;
        try{
            System.out.println("Creating Callable statement...");

            c.setAutoCommit(false);
            CallableStatement proc = c.prepareCall(Constant.getCallJ);
            proc.registerOutParameter(1, Types.BOOLEAN);
            proc.setInt(2, id);
            proc.setInt(3, zip);
            proc.setString(4, name);
            proc.setString(5, coda);
            proc.execute();

            result = proc.getBoolean(1);
            proc.close();
            //put it back to true
            c.setAutoCommit(true);
        }
        catch (SQLException s) {
            s.printStackTrace();
            return false;
        }
        catch (Exception e){
            e.printStackTrace();
            return false;
        }
        return result;
    }

}
