package Controller;

import View.*;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Julien Lugard on 4/28/17.
 */
public class ToolBarController2 implements ActionListener
{
    private JFrame parent;

    public void setParent(JFrame parent)
    {
        this.parent = parent;
    }
    @Override
    public void actionPerformed(ActionEvent evt)
    {
         String command = evt.getActionCommand();
        if (command.equals("ROADWAYS"))
            new ViewUpdateR(parent, "Update Roads");
        if (command.equals("HIGHWAYS"))
            new ViewUpdateH(parent, "Update Highways");
        if (command.equals("INTERCONNECTIONS"))
            new ViewUpdateI(parent, "Update Interconnection");
        if (command.equals("SERVICED CITIES"))
            new ViewUpdateJ(parent, "Update joined city");
        if (command.equals("GET INFO CITY"))
            new ViewDialogInfoCity(parent, "Get Info from City");
        if (command.equals("SCA ROADS"))
            new ViewDiplaySca(parent, "Sca Informations");
        if (command.equals("SEARCH"))
            new ViewSearch(parent, "Search Path");

    }
}
