package Controller;

import Model.Model;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Julien Lugard on 4/29/17.
 */
public class DialogInfoCity implements ActionListener
{
     //ATTRIBUTES
    private JComboBox pkBox;
    private JButton update;
    private JDialog dialog;

    //CONSTRUCTOR
    public DialogInfoCity(JComboBox box, JButton update, JDialog dialog)
    {
        this.pkBox = box;
        this.update = update;
        this.dialog = dialog;
    }

    //METHODES
    @Override
     public void actionPerformed(ActionEvent evt)
    {
        String command = evt.getActionCommand();
        if (command.equals("Update"))
        {
            Model.getInfoFromCity(pkBox.getSelectedItem().toString());
            JOptionPane.showMessageDialog(null,
                        "The datas hsa been retrieved successfully", "Success",
                        JOptionPane.INFORMATION_MESSAGE);
            dialog.setVisible(false);
            dialog.dispose();
        }
    }
}
