package Controller;

import Model.Model;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Julien Lugard on 4/29/17.
 */
public class DialogDisplaySca implements ActionListener
{
    //ATTRIBUTES
    private JComboBox pkBox;
    private JDialog dialog;

    //CONSTRUCTOR
    public DialogDisplaySca(JComboBox box, JDialog dialog)
    {
        this.pkBox = box;
        this.dialog = dialog;
    }

    //METHODES
    @Override
     public void actionPerformed(ActionEvent evt)
    {
        String command = evt.getActionCommand();
        if (command.equals("Update"))
        {
            Model.getInfoSca(pkBox.getSelectedItem().toString());
            JOptionPane.showMessageDialog(null,
                        "The data has been retrieved successfully", "Success",
                        JOptionPane.INFORMATION_MESSAGE);
            dialog.setVisible(false);
            dialog.dispose();
        }
    }
}
