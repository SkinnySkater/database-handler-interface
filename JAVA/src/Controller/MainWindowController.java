package Controller;

import Model.Model;
import MyUtil.Constant;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

/**
 * * Created by Julien Lugard on 4/27/17.
 */
public class MainWindowController implements ActionListener, WindowListener
{
    //ATTRIBUTE
    private static JLabel statusBar = new JLabel();
    //For the Jdialog Connection
    private JTextField user;
    private JPasswordField pwd;
    private JButton okBtn;
    private JDialog dialog;
    private JFrame frame;

    //CONSTRUCTOR
    public MainWindowController()
    {
        super();
    }

    public void setConnectionComponent(JDialog dialog, JTextField user, JPasswordField pwd, JButton ok)
    {
        this.dialog = dialog;
        this.user = user;
        this.pwd  = pwd;
        this.okBtn = ok;
    }

    public static void setStatusBar(JLabel statusB)
    {
        statusBar = statusB;
    }

    public void setMainFraame(JFrame frame)
    {
        this.frame = frame;
    }

    @Override
    public void actionPerformed(ActionEvent evt)
    {
        String command = evt.getActionCommand();
        if (command.equals("Log in"))
        {
            try {
                if (Model.isIsConnected(user.getText().toString(), pwd.getText().toString()))
                {
                    statusBar.setText(Constant.connected);
                    frame.setFocusable(true);
                    dialog.setVisible(false);
                    dialog.dispose();
                }
                else {
                    JOptionPane.showMessageDialog(null,
                            "The Credentials are wrong, please try again", "Error",
                            JOptionPane.ERROR_MESSAGE);
                }
            }
            catch (Exception e) {
                JOptionPane.showMessageDialog(null,
                        "An error happened, please try again", "Error",
                        JOptionPane.ERROR_MESSAGE);
            }
        }
    }


    /**
     * WINDOWS LISTENER
     */
    @Override
    public void windowClosed(WindowEvent e) {
        System.out.println("windowClosed");
        try {
            Model.c.close();
            statusBar.setText(Constant.disconnected);
            System.out.println("Closing all connections");
        }catch (Exception ex){}
    }

    public void windowActivated(WindowEvent e) {
        System.out.println("windowActivated");
        if (!Model.isConnected)
            System.exit(0);
    }

    public void windowClosing(WindowEvent e) {

        int n = JOptionPane.showConfirmDialog(frame
                , "Do you want to quit?"
                , "Exit",
        JOptionPane.YES_NO_OPTION);
        System.out.println(n);
        if (n == 0) {
            try {
                System.exit(0);
                Model.c.close();
                System.out.println("Closing all connections");
            } catch (Exception ex) {
            }
        }
    }

    public void windowDeactivated(WindowEvent e) {
        System.out.println("windowDeactivated");
    }

    public void windowDeiconified(WindowEvent e) {
        System.out.println("windowDeiconified");
    }

    public void windowIconified(WindowEvent e) {
        System.out.println("windowIconified");
    }

    public void windowOpened(WindowEvent e) {
        System.out.println("windowOpened");
    }
}
