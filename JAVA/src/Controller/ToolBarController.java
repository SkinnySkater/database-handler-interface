package Controller;

import Model.Model;
import View.ViewUpdateH;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Julien Lugard on 4/28/17.
 */
public class ToolBarController implements ActionListener
{

    @Override
    public void actionPerformed(ActionEvent evt)
    {
         String command = evt.getActionCommand();
        if (command.equals("ROADWAYS"))
            Model.displayRoads();
        if (command.equals("HIGHWAYS"))
            Model.displayHighways();
        if (command.equals("INTERCONNECTIONS"))
            Model.displayInter();
        if (command.equals("SERVICED CITIES"))
            Model.displaySc();
        if (command.equals("CLEAR"))
            Model.Clear();
    }
}
