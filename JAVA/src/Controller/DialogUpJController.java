package Controller;

import Model.Model;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Julien Lugard on 4/29/17.
 */
public class DialogUpJController implements ActionListener
{
      //ATTRIBUTES
    private JComboBox pkBox;
    private JTextField txt1;
    private JTextField txt2;
    private JTextField txt3;
    private JButton update;
    private JDialog dialog;

    //CONSTRUCTOR
    public DialogUpJController(JComboBox box
            , JTextField txt1
            , JTextField txt2
            , JTextField txt3
            , JButton update
            , JDialog dialog)
    {
        this.pkBox = box;
        this.txt1 = txt1;
        this.txt2 = txt2;
        this.txt3 = txt3;
        this.update = update;
        this.dialog = dialog;
    }

    //METHODES
    @Override
     public void actionPerformed(ActionEvent evt)
    {
        String command = evt.getActionCommand();
        if (command.equals("Update"))
        {
            boolean res = Model.UpdateJ(Integer.parseInt(pkBox.getSelectedItem().toString())
                    , Integer.parseInt(txt1.getText().toString())
                    , txt2.getText().toString().toUpperCase()
                    , txt3.getText().toString().toUpperCase());
            if (res)
            {
                 JOptionPane.showMessageDialog(null,
                        "The Table has been Updated successfully", "Success",
                        JOptionPane.INFORMATION_MESSAGE);
                 dialog.setVisible(false);
                 dialog.dispose();
            }
            else {
                 JOptionPane.showMessageDialog(null,
                        "An error happened, please try again", "Error",
                        JOptionPane.ERROR_MESSAGE);
            }
        }
    }
}
