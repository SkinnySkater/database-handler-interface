package Controller;

import Model.Model;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Julien Lugard on 4/29/17.
 */
public class DialogSearchController implements ActionListener
{
    //ATTRIBUTES
    private JComboBox Box1;
    private JComboBox Box2;
    private JDialog dialog;

    //CONSTRUCTOR
    public DialogSearchController(JComboBox box1, JComboBox box2, JDialog dialog)
    {
        this.Box1 = box1;
        this.Box2 = box2;
        this.dialog = dialog;
    }

    //METHODES
    @Override
     public void actionPerformed(ActionEvent evt)
    {
        String command = evt.getActionCommand();
        if (command.equals("Update"))
        {
            Model.getInfoSearch(Box1.getSelectedItem().toString(), Box2.getSelectedItem().toString());
            JOptionPane.showMessageDialog(null,
                        "The data has been retrieved successfully", "Success",
                        JOptionPane.INFORMATION_MESSAGE);
            dialog.setVisible(false);
            dialog.dispose();
        }
    }
}
