package MyUtil;

/**
 * Created by Julien Lugard on 4/27/17.
 */
public class Constant
{
    public static String DB_URL = "jdbc:postgresql://localhost:5432/testdb";
    public static String connected = "Status: You are currently connected to ";
    public static String disconnected = "Status: Not Connected";

    /**
     * STORED SQL QUERY
     */
    public static String displayH = "SELECT * FROM highway";
    public static String displayR = "SELECT * FROM roads";
    public static String displayI = "SELECT * FROM exitcity";
    public static String displayJ = "SELECT * FROM joincity";
    public static String getInfoCity(String city)
    {
        return "SELECT joincity.coda AS coda, exitcity.libelle AS libelle FROM joincity"
                + " INNER JOIN highway  ON highway.coda = joincity.coda"
                + " INNER JOIN exitcity ON exitcity.coda = highway.coda"
                + " WHERE joincity.name = '" + city + "';";
    }
    public static String getScaInfo(String name)
    {
        return "SELECT roads.codt"
                + " , f_my_date_diff(current_date, sca.contract_dur) AS duration"
                + " , 100 * (roads.pgaukm + roads.pgdukm) AS earn"
                + " FROM roads" +" INNER JOIN highway  ON highway.coda = roads.coda"
                + " INNER JOIN sca  ON sca.code = highway.sca_code"
                + " WHERE sca.name = '"+ name + "';";
    }

    public static String getSearchInfo(String city1, String city2)
    {
        return "SELECT roads.codt FROM roads"
  			   + " INNER JOIN highway  ON highway.coda = roads.coda"
  			   + " INNER JOIN joincity ON joincity.coda = highway.coda"
  			   + " INNER JOIN exitcity ON exitcity.coda = highway.coda"
  			   + " WHERE exitcity.libelle ='" + city1 + "' OR exitcity.libelle ='" + city2 + "'"
  			   + " EXCEPT"
  			   + " SELECT closedroad.codt FROM closedroad;";
    }

    /**
     * STORED SQL QUERY TO FILL PRIMARY KEY COMBOBOX
     */
    public static String getPkH = "SELECT coda    FROM highway";
    public static String getPkR = "SELECT codt    FROM roads";
    public static String getPkI = "SELECT libelle FROM exitcity";
    public static String getPkJ = "SELECT id      FROM joincity";
    public static String getPkS = "SELECT name    FROM sca";


    /**
     * CREATE SQL QUERY FOR UPDATE TABLES
     */
    public static String getCallH = "{ ? = call f_update_highway(?,?,?,?) }";
    public static String getCallR = "{ ? = call f_update_roads(?,?,?,?) }";
    public static String getCallI = "{ ? = call f_update_exitcity(?,?,?) }";
    public static String getCallJ = "{ ? = call f_update_joincity(?,?,?,?) }";
}
