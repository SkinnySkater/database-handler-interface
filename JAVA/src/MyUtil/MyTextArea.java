package MyUtil;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Julien Lugard on 4/28/17.
 */
public class MyTextArea extends JTextArea {

    private ImageIcon img;

    public MyTextArea(int a, int b) {
        super(a,b);
            img = new ImageIcon("src/View/splash/images/splash4.jpg");
    }

    @Override
    protected void paintComponent(Graphics g) {

        g.drawImage(img.getImage(),0,0,null);
        super.paintComponent(g);
    }

}
