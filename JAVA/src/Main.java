import View.splash.SplashScreen;

import javax.swing.*;

/**
 * Created by Julien Lugard on 4/27/17.
 */
public class Main
{
    public static void main(String[] args) throws Exception{
        //Display splashscreen
        SplashScreen splash = new SplashScreen();
        splash.setVisible(true);
        Thread t = Thread.currentThread();
        t.sleep(10000);
        splash.dispose();
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                try {
                    createAndShowGUI();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public static void createAndShowGUI() throws Exception {
        new View.View();
    }

}
